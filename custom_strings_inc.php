<?php
# Translation for Custom Status Code: testing
switch( $g_active_language ) {

	case 'portuguese_brazil':
		$s_status_enum_string = '10:novo,20:retorno,30:refatoracao,40:aprovado,50:atribuido,60:impedimento,70:teste,80:revisao,90:fechado';

		$s_teste_bug_title = 'Mover para o teste';
		$s_teste_bug_button = 'OK';
		$s_email_notification_title_for_status_bug_teste = 'O mantis est� pronto para TESTE.';

		$s_impedimento_bug_title = 'Mover para impedimento';
		$s_impedimento_bug_button = 'OK';
		$s_email_notification_title_for_status_bug_impedimento = 'O mantis est� em IMPEDIMENTO.';
		break;

	default: # english

		$s_status_enum_string = '10:new,20:reproved,30:refactoring,40:aproved,50:assigned,60:stop,70:testing,80:review,90:closed';


		$s_testing_bug_title = 'Mark issue Ready for Testing';
		$s_testing_bug_button = 'Ready for Testing';

		$s_email_notification_title_for_status_bug_testing = 'The following issue is ready for TESTING.';

		$s_impedimento_bug_title = 'Mover para impedimento';
		$s_impedimento_bug_button = 'OK';
		$s_email_notification_title_for_status_bug_impedimento = 'O mantis est� em IMPEDIMENTO.';
		break;
}